/*
 * @Author: liwenlong
 * @Date: 2019-10-20 11:49:15
 * @LastEditors: liwenlong
 * @LastEditTime: 2019-10-29 11:16:07
 */

require('./index.css');
require('page/common/nav/index.js');
require('page/common/header/index.js');
require('util/slider/index.js');
var navSide         = require('page/common/nav-side/index.js');
var templateBanner  = require('./banner.string');
var _mm             = require('util/mm.js');

var page = {
    init: function(){
        this.onLoad();
        this.bindEvent();
    },
    onLoad: function(){
        // 渲染banner的html
        var bannerHtml  = _mm.renderHtml(templateBanner),
            track = '',
            listArr = [];
        $('.banner-con').html(bannerHtml);
        // 计算banner个数，生成track
        for(var i = 0, len = $('.banner-list>li').length; i < len; i ++){
            if(i < len-2){
                $('.banner-list>li').eq(i).addClass('list' + (i+1));
                listArr.push('list' + (i+1));
            }
            // 最后两张图片添加class
            else{
                $('.banner-list>li').eq(i).addClass('last' + (len-i));
                listArr.push('last' + (len-i));
            }
            track += '<i></i>';
        }
        $('.track').html(track).find('i:eq(0)').addClass('focus');
        // banner 动画
        this.bannerMove(listArr);
    },
    // 导航条动画效果
    bindEvent: function(){
        var $keywords_item = $('.keywords-item'),
            $item_con = $('.item-con');
        $keywords_item.hover(function(){
            var index = $(this).index()
            $('.item-con>div').css('display', 'none').eq(index).css('display', 'block')
            $item_con.css('height', '200px');
        }, function(){
            $item_con.css('height', '0px');
        })
        $item_con.hover(function(){
            $item_con.css('height', '200px');
        },function(){
            $item_con.css('height', '0px');
        })
    },
    // 轮播图动画效果
    bannerMove: function(arr){
        var arrlength = arr.length - 1,
            index = 0,
            timer;
        function move(target){
            $('.banner-list li').each(function(index, item){
                $(item).attr({'class': arr[index]})
            })
            index += target;
            $('.track i').removeClass('focus').eq(index % arr.length).addClass('focus');
        }
        function rightMove(){
            arr.unshift(arr[arrlength]);
            arr.pop();
            move(1);
        }
        function leftMove(){
            arr.push(arr[0]);
            arr.shift();
            move(-1);
        }
        function play(){
            timer = setInterval(function(){
                rightMove();
            }, 4000);
        }
        $('.rightBut').click(function(){
            if(!$(this).data('fadeOut')){
                clearInterval(timer);
                rightMove();
                $(this).data('fadeOut', true)
                setTimeout(() => {
                    $(this).data('fadeOut', false)
                }, 600);
                play();
            }
        })
        $('.leftBut').click(function(){
            if(!$(this).data('fadeOut')){
                clearInterval(timer);
                leftMove();
                $(this).data('fadeOut', true)
                setTimeout(() => {
                    $(this).data('fadeOut', false)
                }, 600);
                play();
            }
        })
        $('.track i').on('click', function(){
            var dirLength = $(this).index() - Math.abs(index % arr.length);
            clearInterval(timer);
            if(dirLength > 0){
                for(var i = 0; i < dirLength; i ++){
                    rightMove();
                }
                play();
            }
            else{
                for(var i = 0; i < -dirLength; i ++){
                    leftMove();
                }
                play();
            }
        })
        play();
    }
}
$(function() {
    page.init();
});
